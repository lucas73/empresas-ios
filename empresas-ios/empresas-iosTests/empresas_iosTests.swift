//
//  empresas_iosTests.swift
//  empresas-iosTests
//
//  Created by Lucas Araujo on 28/11/20.
//

import XCTest
@testable import empresas_ios

class empresas_iosTests: XCTestCase {

    // Teste de API e decoding para login
    func testCorrectLoginAuthHeaders() throws {
        
        let navigation = UINavigationController()
        let coordinator = Coordinator(navigation: navigation)
        let presenter = LoginPresenter(coordinator: coordinator)
        
        let authHeadersExpectation = expectation(description: "Login auth headers")
        
        // Correct login
        presenter.clickLogin(email: "testeapple@ioasys.com.br", password: "12341234")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if AuthService.shared.authParameters?.count == 3 {
                authHeadersExpectation.fulfill()
            }
        }
        
        wait(for: [authHeadersExpectation], timeout: 3.0)
    }
    
    // Teste de API e decoding para login
    func testIncorrectLoginAuthHeaders() throws {
        
        let navigation = UINavigationController()
        let coordinator = Coordinator(navigation: navigation)
        let presenter = LoginPresenter(coordinator: coordinator)
        
        let authHeadersExpectation = expectation(description: "Login withouth auth headers")
        
        // Correct login
        presenter.clickLogin(email: "wrong.email", password: "not a password")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if AuthService.shared.authParameters == nil {
                authHeadersExpectation.fulfill()
            }
        }
        
        wait(for: [authHeadersExpectation], timeout: 4.0)
    }
}
