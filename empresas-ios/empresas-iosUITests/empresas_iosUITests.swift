//
//  empresas_iosUITests.swift
//  empresas-iosUITests
//
//  Created by Lucas Araujo on 28/11/20.
//

import XCTest

class empresas_iosUITests: XCTestCase {
    
    func testCorrectLogin() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        sleep(5)

        app.loginEmail.tap()
        app.loginEmail.typeText("testeapple@ioasys.com.br")

        app.loginPassword.tap()
        app.loginPassword.typeText("12341234")

        app.loginEntrar.tap()
        
        sleep(3)

        XCTAssertTrue(app.buscaText.exists)
    }
    
    func testIncorrectLogin() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        sleep(5)

        app.loginEmail.tap()
        app.loginEmail.typeText("wrongemail@ioasys.not.br")

        app.loginPassword.tap()
        app.loginPassword.typeText("notpassword")

        app.loginEntrar.tap()
        
        sleep(3)

        XCTAssertEqual(app.loginError.label, "Credenciais incorretas")
    }
    
    func testSearchEnterprises() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        sleep(5)

        app.loginEmail.tap()
        app.loginEmail.typeText("testeapple@ioasys.com.br")

        app.loginPassword.tap()
        app.loginPassword.typeText("12341234")

        app.loginEntrar.tap()
        
        sleep(4)

        app.buscaText.tap()
        app.buscaText.typeText("OL")
        
        app.cells.element(boundBy: 1).tap()
        
        sleep(3)
        
        XCTAssertTrue(app.detalheEmpresaTitle.exists)
    }
    
    func testSearchEnterprisesNotFound() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        sleep(5)

        app.loginEmail.tap()
        app.loginEmail.typeText("testeapple@ioasys.com.br")

        app.loginPassword.tap()
        app.loginPassword.typeText("12341234")

        app.loginEntrar.tap()
        
        sleep(4)

        app.buscaText.tap()
        app.buscaText.typeText("-HELLow woRLD-")
        
        sleep(3)
        
        XCTAssertEqual(app.buscaNaoEncontrado.label, "Nenhum resultado encontrado")
    }
}

fileprivate extension XCUIApplication {
    var loginEmail: XCUIElement { textFields["login-email"]  }
    var loginPassword: XCUIElement { secureTextFields["login-password"] }
    var loginError: XCUIElement { staticTexts["login-error"]}
    var loginEntrar: XCUIElement { self.buttons["login-entrar"] }
    var buscaText: XCUIElement { self.textFields["busca-text"] }
    var detalheEmpresaTitle: XCUIElement { staticTexts["detalhe-empresa-title"] }
    var buscaNaoEncontrado: XCUIElement { staticTexts["busca-empresas-nao-encontrado"] }
}
