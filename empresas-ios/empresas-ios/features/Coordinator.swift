//
//  Coordinator.swift
//  empresas-ios
//
//  Created by Lucas Araujo on 28/11/20.
//

import Foundation
import UIKit

/// Coordinator basico, apenas para fazer o controle inicial
class Coordinator {
    
    let navigation: UINavigationController
    
    init(navigation: UINavigationController) {
        self.navigation = navigation
    }
    
    func irParaLogin() {
        if let controller = UIStoryboard(name: "Login", bundle: nil).instantiateInitialViewController() as? LoginViewController {
            let presenter = LoginPresenter(coordinator: self)
            controller.presenter = presenter
            presenter.reloader = controller
            navigation.pushViewController(controller, animated: true)
        }
    }
    
    func irParaBuscaEmpresas() {
        if let controller = UIStoryboard(name: "BuscaEmpresas", bundle: nil).instantiateInitialViewController() as? BuscaEmpresasViewController {
            let presenter = ListaEmpresasPresenter(coordinator: self)
            controller.presenter = presenter
            presenter.reloader = controller
            navigation.pushViewController(controller, animated: true)
        }
    }
    
    func irParaDetalheEmpresa(detalhe: EmpresasService.GETDetalheEmpresa) {
        if let controller = UIStoryboard(name: "DetalheEmpresa", bundle: nil).instantiateInitialViewController() as? DetalheEmpresaViewController {
            controller.empresa = detalhe
            navigation.pushViewController(controller, animated: true)
        }
    }
    
}
