//
//  ListaEmpresasPresenter.swift
//  empresas-ios
//
//  Created by Lucas Araujo on 28/11/20.
//

import Foundation

class ListaEmpresasPresenter {
    
    // Respostas do GET empresas
    var service = EmpresasService()
    var empresasPesquisadas = [EmpresasService.Empresa]()
    var error: Error?
    
    var reloader: PresenterReloader?
    
    // Coordinator
    let coordinator: Coordinator
    
    init(coordinator: Coordinator) {
        self.coordinator = coordinator
    }
    
    func atualizaPequisaEmpresas(por nome: String?) {
        
        Loading.start()
        
        service.getEmpresas(nome_empresa: nome).validate().responseString { response in
            
            Loading.stop()
            
            switch response.result {
            case .success(let json):
                
                // Limpa erro
                self.error = nil
                
                // Atualiza empresas pesquisadas
                if let empresas = json.decodeJSON(type: EmpresasService.GETEmpresas.self) {
                    self.empresasPesquisadas = empresas.enterprises ?? []
                }
                
            case .failure(let error):
                self.empresasPesquisadas = []
                self.error = error
            }
            
            // Atualiza informacoes
            self.reloader?.reload()
        }
    }
    
    func selecionouEmpresa(_ empresa: EmpresasService.Empresa) {
        
        Loading.start()
        
        service.getDetalheEmpresa(id: empresa.id ?? 0).validate().responseString { response in
            
            Loading.stop()
            
            switch response.result {
            case .success(let json):
                // ir para detalhes
                if let detalheEmpresa = json.decodeJSON(type: EmpresasService.GETDetalheEmpresa.self) {
                    self.coordinator.irParaDetalheEmpresa(detalhe: detalheEmpresa)
                }
            case .failure(let error):
                debugPrint("[!] Falha", error)
            }
        }
    }
}
