//
//  BuscaEmpresasViewController.swift
//  empresas-ios
//
//  Created by Lucas Araujo on 28/11/20.
//

import Foundation
import UIKit

class BuscaEmpresasViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var presenter: ListaEmpresasPresenter?
    
    // Table
    @IBOutlet weak var empresasTable: UITableView!
    // Search
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    // Header
    @IBOutlet weak var headerBackgroundHeight: NSLayoutConstraint!
    @IBOutlet weak var headerBackground: UIImageView!
    @IBOutlet weak var mensagemNaoEncontradoLabel: UILabel!
    
    // Search timer
    var searchTimer: Timer?
    
    var headerColapsed: Bool = false {
        didSet {
            if headerColapsed == true && oldValue == false {
                headerBackgroundHeight.constant = 80
                UIView.animate(withDuration: 0.8, delay: 0.1, options: .curveEaseInOut) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configuracao da table
        empresasTable.delegate = self
        empresasTable.dataSource = self
        empresasTable.separatorStyle = .none
        empresasTable.rowHeight = UITableView.automaticDimension
        
        registrarTableViewCells()
        
        // Configuracao do search
        searchView.layer.cornerRadius = 6
        searchTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc
    func timerTrigger() {
        
        // Limpa timer
        searchTimer?.invalidate()
        searchTimer = nil
        
        if searchTextField.text?.isEmpty == true {
            // limpa pesquisa
            presenter?.empresasPesquisadas = []
            reload()
        } else {
            // Realiza pesquisa
            presenter?.atualizaPequisaEmpresas(por: searchTextField.text)
        }
    }
}

extension BuscaEmpresasViewController: UITableViewDelegate, UITableViewDataSource {
    
    func registrarTableViewCells() {
        empresasTable.register(UINib(nibName: "CardEmpresaTableViewCell", bundle: nil),
                                     forCellReuseIdentifier: "CardEmpresaTableViewCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (presenter?.empresasPesquisadas ?? []).count == 0 && searchTextField.text?.isEmpty == false {
            return 1
        } else {
            return (presenter?.empresasPesquisadas.count ?? 0) + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (presenter?.empresasPesquisadas ?? []).count == 0 && searchTextField.text?.isEmpty == false {
            let cell = UITableViewCell()
            cell.textLabel?.text = "Nenhum resultado encontrado"
            cell.textLabel?.accessibilityIdentifier = "busca-empresas-nao-encontrado"
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.font = UIFont(name: Font.light, size: 16)
            return cell
        } else {
            
            if searchTextField.text?.isEmpty == false {
                // Primeira celula é a célula de resultados encontrados
                if indexPath.row == 0 {
                    let cell = UITableViewCell()
                    cell.textLabel?.text = "\(presenter?.empresasPesquisadas.count ?? 0) resultados encontrados"
                    cell.textLabel?.textAlignment = .left
                    cell.textLabel?.font = UIFont(name: Font.light, size: 14)
                    return cell
                }
                
                // Configuracao de celula de empresas
                if let empresa = presenter?.empresasPesquisadas[indexPath.row - 1] {
                    if let cell = tableView.dequeueReusableCell(withIdentifier: "CardEmpresaTableViewCell") as? CardEmpresaTableViewCell {
                        cell.setupCell(empresa: empresa)
                        return cell
                    }
                }
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row > 0 {
            if let empresa = presenter?.empresasPesquisadas[indexPath.row - 1] {
                presenter?.selecionouEmpresa(empresa)
            }
        }
    }
    
}

extension BuscaEmpresasViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // Caso tenha finalizado edicao e timer ainda esteja ativo
        if searchTimer != nil {
            // realiza trigger manual
            timerTrigger()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // Reset timer
        searchTimer?.invalidate()
        searchTimer = nil
        searchTimer = Timer.scheduledTimer(timeInterval: 0.7,
                                           target: self,
                                           selector: #selector(self.timerTrigger),
                                           userInfo: nil,
                                           repeats: false)
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        headerColapsed = true
        return true
    }
}

extension BuscaEmpresasViewController: PresenterReloader {
    func reload() {
        empresasTable.reloadData()
    }
}
