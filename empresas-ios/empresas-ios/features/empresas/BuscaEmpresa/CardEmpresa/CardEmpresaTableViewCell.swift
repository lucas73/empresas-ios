//
//  CardEmpresaTableViewCell.swift
//  empresas-ios
//
//  Created by Lucas Araujo on 29/11/20.
//

import UIKit

class CardEmpresaTableViewCell: UITableViewCell {
    @IBOutlet weak var empresaImagem: UIImageView!
    @IBOutlet weak var nomeEmpresaLabel: UILabel!
    
    func setupCell(empresa: EmpresasService.Empresa) {
        self.nomeEmpresaLabel.text = empresa.enterpriseName
        let url = "\(Domains.base)\(empresa.photo ?? "")"
        self.empresaImagem.alpha = 0
        ImageDownload.download(url: url) { [weak self] image in
            self?.empresaImagem.image = image
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut) {
                self?.empresaImagem.alpha = 1
                self?.layoutIfNeeded()
            }
        }
    }
}
