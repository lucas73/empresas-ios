//
//  DetalheEmpresaViewController.swift
//  empresas-ios
//
//  Created by Lucas Araujo on 29/11/20.
//

import Foundation
import UIKit

class DetalheEmpresaViewController: UIViewController {
    
    var empresa: EmpresasService.GETDetalheEmpresa?
    
    @IBOutlet weak var detalheEmpresaTable: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    var downloadedImage: UIImage?
    
    var imageDownloadCompletion: ((UIImage?) -> Void)? {
        didSet {
            if downloadedImage != nil {
                imageDownloadCompletion?(downloadedImage)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configuracao da table
        detalheEmpresaTable.delegate = self
        detalheEmpresaTable.dataSource = self
        detalheEmpresaTable.separatorStyle = .none
        detalheEmpresaTable.allowsSelection = false
        detalheEmpresaTable.rowHeight = UITableView.automaticDimension
        
        registerCells()
        
        // Titulo
        titleLabel.text = empresa?.enterprise?.enterpriseName
        titleLabel.font = UIFont(name: Font.bold, size: 20)
        
        // Download da imagem
        downloadImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        imageDownloadCompletion = nil
    }
    
    @IBAction func voltarButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func downloadImage() {
        if let photo = empresa?.enterprise?.photo {
            let url = "\(Domains.base)\(photo)"
            
            // Baixando a imagem asincronamente
            ImageDownload.download(url: url) { [weak self] downloadImage in
                
                // guardando imagem
                if let newImage = downloadImage {
                    self?.downloadedImage = newImage
                    if self?.imageDownloadCompletion != nil {
                        self?.imageDownloadCompletion?(newImage)
                    }
                }
            }
        }
    }
    
}

extension DetalheEmpresaViewController: UITableViewDelegate, UITableViewDataSource {
    
    func registerCells() {
        detalheEmpresaTable.register(UINib(nibName: "CardImagemTableViewCell", bundle: nil),
                                     forCellReuseIdentifier: "CardImagemTableViewCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CardImagemTableViewCell") as? CardImagemTableViewCell {
                
                // animacao de download
                if downloadedImage == nil {
                    cell.imageBackground?.alpha = 0
                    
                    // aguardando callback de download
                    imageDownloadCompletion = { [weak cell] downloadImage in
                        
                        // adicionando imagem na celula
                        if let newImage = downloadImage {
                            cell?.imageBackground?.image = newImage
                            
                            // Animando entrada da imagem
                            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut) {
                                cell?.imageBackground?.alpha = 1
                                cell?.layoutIfNeeded()
                            }
                        }
                    }
                } else {
                    // imagem ja carregada
                    cell.imageBackground?.image = downloadedImage
                }
                
                return cell
            }
            
        case 1:
            let cell = UITableViewCell()
            cell.textLabel?.text = empresa?.enterprise?.enterpriseDescription
            cell.textLabel?.font = UIFont(name: Font.light, size: 16)
            cell.textLabel?.numberOfLines = 0
            return cell
            
        default:
            break
        }
        
        return UITableViewCell()
    }
    
    
    
    
}
