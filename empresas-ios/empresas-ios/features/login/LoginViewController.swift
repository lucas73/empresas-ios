//
//  LoginViewController.swift
//  empresas-ios
//
//  Created by Lucas Araujo on 28/11/20.
//

import UIKit

class LoginViewController: UIViewController {
    
    var presenter: LoginPresenter?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var headerColapsed: Bool = false {
        didSet {
            
            if headerColapsed == true && oldValue == false {
                // Colapsando header
                self.headerBackgroundHeightConstraint.constant = 200
                self.headerIconTopConstraint.constant = 20
                UIView.animate(withDuration: 0.7, delay: 0.1, options: .curveEaseInOut, animations: {
                    self.view.layoutIfNeeded()
                    self.headerLabel.alpha = 0
                }, completion: { _ in
                    self.headerLabel.isHidden = true
                })
            }
            
            if headerColapsed == false {
                self.headerBackgroundHeightConstraint.constant = 350
                self.headerIconTopConstraint.constant = 80
                self.headerLabel.isHidden = false
                UIView.animate(withDuration: 0.7, delay: 0.1, options: .curveEaseInOut) {
                    self.view.layoutIfNeeded()
                    self.headerLabel.alpha = 1
                }
            }
        }
    }
    
    // Header
    @IBOutlet weak var topHeaderBackground: UIImageView!
    @IBOutlet weak var headerIconTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerBackgroundHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerLabel: UILabel!
    
    // Email textfield
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailTextFieldError: UILabel!
    
    // Password textfield
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordTextFieldError: UILabel!
    
    // Botao entrar
    @IBOutlet weak var entrarButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        entrarButton.layer.cornerRadius = 6
        emailTextField.delegate = self
        passwordTextField.delegate = self
        (self.view as? UIScrollView)?.delegate = self
        
        // Accessibility identifiers
        passwordTextField.accessibilityIdentifier = "login-password"
        passwordTextFieldError.accessibilityIdentifier = "login-error"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func clickEntrar(_ sender: Any) {
        presenter?.clickLogin(email: emailTextField.text ?? "",
                              password: passwordTextField.text ?? "")
    }
}

extension LoginViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // Limitando uso de scroll
        if scrollView.contentOffset.y < -20 {
            scrollView.contentOffset.y = -20
        }
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        headerColapsed = true
        return true
    }
}

extension LoginViewController: PresenterReloader {
    
    func reload() {
        if presenter?.error != nil {
            passwordTextFieldError.isHidden = false
            passwordTextFieldError.text = "Credenciais incorretas"
        } else {
            passwordTextFieldError.isHidden = true
        }
    }
}

