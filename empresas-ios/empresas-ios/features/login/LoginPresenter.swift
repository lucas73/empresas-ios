//
//  LoginPresenter.swift
//  empresas-ios
//
//  Created by Lucas Araujo on 28/11/20.
//

import Foundation
import Alamofire

class LoginPresenter {
    
    var service = LoginService()
    var reloader: PresenterReloader?
    var error: Error?
    
    // Coordinator
    let coordinator: Coordinator
    
    init(coordinator: Coordinator) {
        self.coordinator = coordinator
    }
    
    func clickLogin(email: String, password: String) {
        
        // Ativa loading
        Loading.start()
        
        // Realiza requisicao
        let request = service.requestLogin(email: email, password: password)
        
        // Adiciona validacoes basicas
        request.validate(statusCode: 200..<300)
               .validate(contentType: ["application/json"])
        
        // Tratamento da response
        request.responseJSON { response in
            
            // Desativa loading
            Loading.stop()
            
            switch response.result {
            case .success(_):
                
                // Limpa erro
                self.error = nil
                
                // Atualiza credenciais de autorizacao
                AuthService.shared.setAuthorizationParameters(basedOn: response.response)
                
                // Segue com fluxo do app
                self.entrarNoApp()
                
            case .failure(let failure):
                self.error = failure
                self.reloader?.reload()
            }
        }
    }
    
    func entrarNoApp() {
         coordinator.irParaBuscaEmpresas()
    }
    
}
