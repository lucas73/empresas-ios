//
//  PresenterReloader.swift
//  empresas-ios
//
//  Created by Lucas Araujo on 28/11/20.
//

import Foundation

/// Delegate de presenter para informar views para atualizarem
protocol PresenterReloader {
    func reload()
}
