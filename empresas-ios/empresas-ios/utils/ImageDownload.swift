//
//  ImageDownload.swift
//  empresas-ios
//
//  Created by Lucas Araujo on 29/11/20.
//

import Foundation
import UIKit
import Alamofire

struct ImageDownload {
    
    static func download(url: String, completion: @escaping ( (UIImage?) -> Void) ) {
        AF.request(url, method: .get).response { response in
            if let data = response.data {
                let image = UIImage(data: data, scale:1)
                completion(image)
            } else {
                completion(nil)
            }
        }
    }
}
