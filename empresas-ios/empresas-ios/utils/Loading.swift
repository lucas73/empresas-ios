//
//  Loading.swift
//  empresas-ios
//
//  Created by Lucas Araujo on 29/11/20.
//

import Foundation
import UIKit

struct Loading {
    static private var loadingCount: Int = 0
    static private var activity: UIActivityIndicatorView = UIActivityIndicatorView()
    static private var container: UIView = UIView()
    static private var loadingView: UIView = UIView()
    
    static func start() {
        if self.loadingCount == 0 {
            self.addSpinner()
        }
        
        self.loadingCount += 1
    }
    
    private static func addSpinner() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let mainView = appDelegate.coordinator?.navigation.view else { return }
        
        self.container.frame = mainView.frame
        self.container.center = mainView.center
        self.container.backgroundColor = UIColor(white: 0xFFFFFF, alpha: 0.3)
        
        self.loadingView.frame = CGRect(x: 0.0, y: 0.0, width: 80.0, height: 80.0)
        self.loadingView.center = mainView.center
        self.loadingView.backgroundColor = UIColor(white: 0x444444, alpha: 0.7)
        self.loadingView.clipsToBounds = true
        self.loadingView.layer.cornerRadius = 10
        
        self.activity.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
        self.activity.style = .whiteLarge
        self.activity.center = CGPoint(x: loadingView.frame.size.width/2, y: loadingView.frame.size.height/2)
        
        self.loadingView.addSubview(activity)
        self.container.addSubview(loadingView)
        
        if self.loadingCount <= 1 {
            fadeInAnimation()
        }
        
        mainView.addSubview(container)
        mainView.bringSubviewToFront(self.container)
        
        self.activity.startAnimating()
        mainView.isUserInteractionEnabled = false
    }
    
    static func stop() {
        self.loadingCount -= 1
        if self.loadingCount < 0 {
            self.loadingCount = 0
        }
        
        guard loadingCount == 0 else { return }

        self.activity.stopAnimating()
        self.container.removeFromSuperview()
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let mainView = appDelegate.coordinator?.navigation.view else { return }
        mainView.isUserInteractionEnabled = true
    }
    
    static func show() {
        let mainView = UIApplication.shared.windows.first
        mainView?.bringSubviewToFront(self.container)
    }
    
    static func hide() {
        let mainView = UIApplication.shared.windows.first
        mainView?.sendSubviewToBack(self.container)
    }
    
    static func fadeInAnimation() {
        self.loadingView.alpha = 0.0
        self.container.alpha = 0.0
        
        UIView.animate(withDuration: 0.05, delay: 0.05, options: .curveEaseInOut, animations: {
            self.loadingView.alpha = 0.2
            self.container.alpha = 0.2
        })
        
        UIView.animate(withDuration: 0.1, delay: 0.1, options: .curveEaseInOut, animations: {
            self.loadingView.alpha = 1
            self.container.alpha = 1
        })
    }
}
