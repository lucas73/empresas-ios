//
//  JSONDecoding.swift
//  empresas-ios
//
//  Created by Lucas Araujo on 28/11/20.
//

import Foundation

extension Data {
    
    func decodeJSON<Type: Decodable>(type: Type.Type) -> Type? {
        return try? JSONDecoder().decode(Type.self, from: self)
    }
}

extension String {
    
    func decodeJSON<Type: Decodable>(type: Type.Type) -> Type? {
        if let data = self.data(using: .utf8) {
            return data.decodeJSON(type: type)
        }
        return nil
    }
}
