//
//  LoginService.swift
//  empresas-ios
//
//  Created by Lucas Araujo on 28/11/20.
//

import Foundation
import Alamofire

class LoginService {
    
    /// Login parameters for POST
    struct LoginPostParameters: Encodable {
        let email: String
        let password: String
    }
    
    /// Executes POST request to login
    /// - Parameters:
    ///     - email: ex.: email@email.com
    ///     - password: ex.: 123123
    /// - Returns: The request
    func requestLogin(email: String, password: String) -> DataRequest {
        
        let url = "\(Domains.api_v1)/users/auth/sign_in"
        let postBody = LoginPostParameters(email: email, password: password)
        
        return AF.request(url, method: .post, parameters: postBody, encoder: JSONParameterEncoder.default)
    }
}
