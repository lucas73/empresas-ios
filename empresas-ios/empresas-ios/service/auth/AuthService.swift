//
//  AuthService.swift
//  empresas-ios
//
//  Created by Lucas Araujo on 28/11/20.
//

import Foundation
import Alamofire

/// Responsible for authentication information
class AuthService {
    
    /// Shared instance for auth headers
    static let shared = AuthService()
    
    /// Contains access-token, client and uid information
    static var authHeaders: HTTPHeaders {
        HTTPHeaders( AuthService.shared.authParameters ?? [String: String]() )
    }
    
    /// Storing auth parameters
    var authParameters: [String: String]?
    
    /// setting current auth parameters
    func setAuthorizationParameters(basedOn response: HTTPURLResponse?) {
        
        guard let token = response?.allHeaderFields["access-token"] as? String,
        let client = response?.allHeaderFields["client"] as? String,
        let uid = response?.allHeaderFields["uid"] as? String else { return }
        
        authParameters = [String: String]()
        authParameters?["access-token"] = token
        authParameters?["client"] = client
        authParameters?["uid"] = uid
    }
}
