//
//  APIDomain.swift
//  empresas-ios
//
//  Created by Lucas Araujo on 28/11/20.
//

import Foundation

struct Domains {
    static let base: String = "https://empresas.ioasys.com.br"
    static let api_v1: String = "\(Domains.base)/api/v1"
}
