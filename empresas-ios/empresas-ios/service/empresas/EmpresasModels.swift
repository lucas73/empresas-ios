//
//  EmpresasModels.swift
//  empresas-ios
//
//  Created by Lucas Araujo on 28/11/20.
//

import Foundation

extension EmpresasService {
    
    struct GETEmpresas: Codable {
        var enterprises: [Empresa]?
    }
    
    struct Empresa: Codable {
        var id: Int?
        var emailEnterprise, facebook, twitter, linkedin: String?
        var phone: String?
        var ownEnterprise: Bool?
        var enterpriseName, photo, enterprisDescription, city: String?
        var country: String?
        var value, sharePrice: Double?
        var enterpriseType: TipoEmpresa?
        
        enum CodingKeys: String, CodingKey {
            case id
            case emailEnterprise = "email_enterprise"
            case facebook, twitter, linkedin, phone
            case ownEnterprise = "own_enterprise"
            case enterpriseName = "enterprise_name"
            case photo
            case enterprisDescription = "description"
            case city, country, value
            case sharePrice = "share_price"
            case enterpriseType = "enterprise_type"
        }
    }

    struct TipoEmpresa: Codable {
        var id: Int?
        var enterpriseTypeName: String?

        enum CodingKeys: String, CodingKey {
            case id
            case enterpriseTypeName = "enterprise_type_name"
        }
    }
    
    struct GETDetalheEmpresa: Codable {
        var enterprise: DetalheEmpresa?
        var success: Bool?
    }
    
    struct DetalheEmpresa: Codable {
        var id: Int?
        var enterpriseName, enterpriseDescription: String?
        var emailEnterprise, facebook, twitter, linkedin: String?
        var phone: String?
        var ownEnterprise: Bool?
        var photo: String?
        var value, shares, sharePrice, ownShares: Double?
        var city, country: String?
        var enterpriseType: TipoEmpresa?
        
        enum CodingKeys: String, CodingKey {
            case id
            case enterpriseName = "enterprise_name"
            case enterpriseDescription = "description"
            case emailEnterprise = "email_enterprise"
            case facebook, twitter, linkedin, phone
            case ownEnterprise = "own_enterprise"
            case photo, value, shares
            case sharePrice = "share_price"
            case ownShares = "own_shares"
            case city, country
            case enterpriseType = "enterprise_type"
        }
    }
}
