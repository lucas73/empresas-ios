//
//  EmpresasService.swift
//  empresas-ios
//
//  Created by Lucas Araujo on 28/11/20.
//

import Foundation
import Alamofire

class EmpresasService {
    
    func getEmpresas(tipo_empresa: String? = nil, nome_empresa: String? = nil) -> DataRequest {
        let url = "\(Domains.api_v1)/enterprises"
        var query = [String: String]()
        
        if let tipo = tipo_empresa {
            query["enterprise_types"] = tipo
        }
        
        if let nome = nome_empresa {
            query["name"] = nome
        }
        
        return AF.request(url, method: .get, parameters: query, headers: AuthService.authHeaders)
    }
    
    func getDetalheEmpresa(id: Int) -> DataRequest {
        let url = "\(Domains.api_v1)/enterprises/\(id)"
        return AF.request(url, method: .get, headers: AuthService.authHeaders)
    }
    
}
