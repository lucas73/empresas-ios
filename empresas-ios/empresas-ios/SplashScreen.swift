//
//  SplashScreen.swift
//  empresas-ios
//
//  Created by Lucas Araujo on 28/11/20.
//

import Foundation
import UIKit

class SplashScreen: UIViewController {
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var verticalCenterConstraint: NSLayoutConstraint!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var coordinator: Coordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        verticalCenterConstraint.constant = 40
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let navigation = self.navigationController {
            coordinator = Coordinator(navigation: navigation)
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                appDelegate.coordinator = coordinator
            }
        }
        
        // Aparecendo
        UIView.animate(withDuration: 1, delay: 0, options: .curveEaseOut) {
            self.verticalCenterConstraint.constant = -20
            self.view.layoutIfNeeded()
        }
        
        // Desaparecendo
        UIView.animate(withDuration: 1, delay: 1.7, options: .curveEaseInOut) {
            self.verticalCenterConstraint.constant = -60
            self.view.layoutIfNeeded()
            self.logoImageView.alpha = 0
        }
        
        // Transitando para login
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.coordinator?.irParaLogin()
        }
    }
    
}
