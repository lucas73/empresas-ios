//
//  Font.swift
//  empresas-ios
//
//  Created by Lucas Araujo on 29/11/20.
//

import Foundation

struct Font {
    static let light = "Rubik-Light"
    static let bold = "Rubik-Bold"
    static let medium = "Rubik-Medium"
    static let regular = "Rubik-Regular"
}
